<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta
			name="viewport"
			content="width=device-width, initial-scale=1.0,maximum-scale=1.0"
		/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />

		<title>SpiceBox</title>

		<!-- Google Fonts -->
		<link
			href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700&display=swap"
			rel="stylesheet"
		/>
		<link
			href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap"
			rel="stylesheet"
		/>

		<!-- Custom CSS -->
		<link rel="stylesheet" href="<?= get_template_directory_uri()?>/assets/css/style.css" />
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"
		/>
		<link
			rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"
        />
	</head>
<div class="hero-banner">
			<img
				src="<?= get_template_directory_uri()?>/assets/img/spicebox_logo.png"
				class="img-fluid brand-logo"
			/>

			<div class="content-container">
				<h1>Craving for good Indian food?</h1>
				<p>
					Look no further — we’re here to bring good vibes to your
					home. Get Kashmir’s signature quality and flavors delivered
					right to your doorstep!
				</p>
			</div>
		</div>

		<div class="cards-container">
			<div
				class="equal-cards"
				style="background-image: url('<?= get_template_directory_uri()?>/assets/img/first.png');"
				id="1"
			></div>
			<div
				class="equal-cards"
				style="background-image: url('<?= get_template_directory_uri()?>/assets/img/second.png');"
				id="2"
			></div>
			<div
				class="equal-cards"
				style="background-image: url('<?= get_template_directory_uri()?>/assets/img/third.png');"
				id="3"
			></div>
		</div>
		<div class="container">
			<div class="form-container">
				<h1 class="form-header">
					Namaste! Kindly fill out the form with your complete
					details.
				</h1>
				<div class="form-input">
					<label for="name">Name*</label>
					<input type="text" id="user_name" name="entry.807147344" />
				</div>
				<div class="form-input">
					<label for="name">Contact Number*</label>
					<input
						type="text"
						id="user_contact_number"
						name="entry.926405120"
					/>
				</div>
				<div class="form-input">
					<label for="name">Address*</label>
					<input
						type="text"
						id="user_address"
						name="entry.991121056"
					/>
				</div>

				<div class="form-inline">
					<div class="pick-up-container">
						<label class="inline-label">Pick Up</label>
						<label for="pick_up_yes" class="radio">
							<input
								type="radio"
								name="entry.74791247"
								id="pick_up_yes"
								class="radio__input entry.74791247"
								value="Yes"
							/>
							<div class="radio__radio"></div>
							Yes
						</label>
						<label for="pick_up_no" class="radio">
							<input
								type="radio"
								name="entry.74791247"
								id="pick_up_no"
								class="radio__input entry.74791247"
								value="No"
								checked
							/>
							<div class="radio__radio"></div>
							No
						</label>
					</div>

					<div class="delivery-container">
						<label class="inline-label">Delivery</label>
						<label for="delivery_yes" class="radio">
							<input
								type="radio"
								name="entry.1839603799"
								id="delivery_yes"
								class="radio__input entry.1839603799"
								value="Yes"
								checked
							/>
							<div class="radio__radio"></div>
							Yes
						</label>
						<label for="delivery_no" class="radio">
							<input
								type="radio"
								name="entry.1839603799"
								id="delivery_no"
								class="radio__input entry.1839603799"
								value="No"
							/>
							<div class="radio__radio"></div>
							No
						</label>
					</div>
				</div>

				<div class="form-input">
					<label for="name">Date of pick-up/delivery</label>
					<input name="entry.231769207" id="item_get_date" />
				</div>
				<div class="form-input">
					<label for="name">Time of pick-up/delivery:</label>
					<input name="entry.1780882638" id="item_get_time" />
				</div>

				<div class="form-input">
					<label for="name">Orders*</label>
					<label class="shipping_note"
						>*Please indicate both order and quantity.
					</label>
					<textarea
						rows="10"
						style="width: 100%;"
						name="entry.587890643"
						id="user_orders"
					></textarea>
				</div>

				<div class="form-input">
					<label>Shipping Method</label>
					<label class="shipping_note"
						>*Customers should shoulder Grab Express or Lalamove
						shipping expenses.</label
					>
					<label for="grab_express" class="radio">
						<input
							type="radio"
							name="entry.230807545"
							id="grab_express"
							class="radio__input entry.230807545"
							value="Grab Express"
						/>
						<div class="radio__radio"></div>
						Grab Express
					</label>

					<label for="kashmir_delivery" class="radio">
						<input
							type="radio"
							name="entry.230807545"
							id="kashmir_delivery"
							class="radio__input entry.230807545"
							value="Kashmir Delivery"
							checked
						/>
						<div class="radio__radio"></div>
						Kashmir Delivery
					</label>

					<label for="lalamove" class="radio">
						<input
							type="radio"
							name="entry.230807545"
							id="lalamove"
							class="radio__input entry.230807545"
							value="Lalamove"
						/>
						<div class="radio__radio"></div>
						Lalamove
					</label>
				</div>

				<div class="form-input">
					<label>Payment Method</label>
					<label class="shipping_note"
						>*We do not accept cash payments.</label
					>
					<label for="Gcash" class="radio payment_radio">
						<input
							type="radio"
							name="entry.1411939492"
							id="Gcash"
							class="radio__input entry.1411939492"
							value="Gcash"
						/>
						<div class="radio__radio"></div>
						Gcash
					</label>

					<label for="UnionBank" class="radio payment_radio">
						<input
							type="radio"
							name="entry.1411939492"
							id="UnionBank"
							class="radio__input entry.1411939492"
							value="UnionBank"
						/>
						<div class="radio__radio"></div>
						UnionBank
					</label>

					<label for="BDO" class="radio payment_radio">
						<input
							type="radio"
							name="entry.1411939492"
							id="BDO"
							class="radio__input entry.1411939492"
							value="BDO"
						/>
						<div class="radio__radio"></div>
						BDO
					</label>
					<label for="swipe_on_delivery" class="radio payment_radio">
						<input
							type="radio"
							name="entry.1411939492"
							id="swipe_on_delivery"
							class="radio__input entry.1411939492"
							value="Swipe on Delivery"
							checked
						/>
						<div class="radio__radio"></div>
						Swipe on Delivery
					</label>
				</div>

				<div class="form-button">
					<button id="submit_order">SUBMIT ORDER</button>
				</div>
			</div>
		</div>

		<div class="faq-container">
			<h1 class="faq-header">Frequently Asked Question</h1>
			<div class="faq-inner-container">
				<div class="faq-info">
					<h2>Do you deliver everywhere in Metro Manila?</h2>
					<p>
						Unfortunately, due to restrictions on travel, we can
						only cater to our guests in Makati and Bonifacio Global
						City.
					</p>
				</div>
				<div class="faq-info">
					<h2>When will my order be delivered?</h2>
					<p>
						To maximize the quality of our dishes, we encourage
						putting in your orders a day ahead. Our delivery hours
						are from 11:30am to 5:30pm daily, but you will be
						notified of your expected delivery time.
					</p>
				</div>
			</div>
			<div class="faq-inner-container">
				<div class="faq-info">
					<h2>
						Is everything on your regular menu available for
						delivery?
					</h2>
					<p>
						We’re currently offering a modified menu of your
						favorite starters and family-style platters good for 3-5
						people.
					</p>
				</div>
				<div class="faq-info">
					<h2>How do we keep food deliveries safe?</h2>
					<p>
						In keeping with social distancing parameters, our team
						will leave your order in the reception lobby of your
						building, or outside the gate of your house after
						ringing the doorbell. We will keep our distance as we
						wait for you to collect your food. The sales invoice and
						delivery receipt will be attached to the bag.
					</p>
				</div>
			</div>
		</div>
		<footer>
			<div class="footer-container">
				<img
					src="<?= get_template_directory_uri()?>/assets/img/spicebox_logo.png"
					class="img-fluid brand-logo"
				/>

				<a
					href="#"
					target="_blank"
					rel="noopener noreferrer"
					class="site-link"
					>www.spicebox.com</a
				>
			</div>
			<div class="footer-container">
				<h2>VISIT US</h2>

				<p>M - F / 10AM - 8PM</p>
				<p>
					Lorem Ipsum, Dolor SIt Amet, Consectetur Ave. Makati,
					Philippines 1232
				</p>
			</div>
			<div class="footer-container">
				<h2>GET IN TOUCH</h2>

				<p>hello@spicebox.com</p>
				<p class="mobile_number">
					(+63) 123 456 7891 (+63) 987 654 3210
				</p>
				<ul>
					<li>
						<svg
							width="26"
							height="26"
							viewBox="0 0 26 26"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								d="M25.2223 13.1613C25.2223 6.29856 19.6603 0.736572 12.7976 0.736572C5.93491 0.736572 0.372925 6.29856 0.372925 13.1613C0.372925 19.3639 4.9157 24.5036 10.8563 25.4355V16.7528H7.70155V13.1613H10.8563V10.4239C10.8563 7.31049 12.7102 5.58996 15.5495 5.58996C16.9084 5.58996 18.3305 5.83263 18.3305 5.83263V8.89027H16.7628C15.2195 8.89027 14.739 9.84881 14.739 10.8316V13.1613H18.1849L17.634 16.7528H14.739V25.4355C20.6795 24.5036 25.2223 19.3639 25.2223 13.1613Z"
								fill="white"
							/>
						</svg>
					</li>
					<li>
						<svg
							width="26"
							height="26"
							viewBox="0 0 26 26"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M12.6865 0.736572C5.8245 0.736572 0.26178 6.29929 0.26178 13.1613C0.26178 20.0232 5.8245 25.5859 12.6865 25.5859C19.5484 25.5859 25.1111 20.0232 25.1111 13.1613C25.1111 6.29929 19.5484 0.736572 12.6865 0.736572ZM9.95473 6.57479C10.6616 6.54263 10.8874 6.53476 12.6871 6.53476H12.685C14.4852 6.53476 14.7103 6.54263 15.4171 6.57479C16.1225 6.6071 16.6043 6.71878 17.0268 6.88265C17.463 7.05177 17.8316 7.27818 18.2002 7.64678C18.5688 8.01511 18.7953 8.38481 18.9651 8.82065C19.128 9.24199 19.2398 9.72352 19.2729 10.429C19.3047 11.1358 19.313 11.3617 19.313 13.1613C19.313 14.961 19.3047 15.1863 19.2729 15.8931C19.2398 16.5983 19.128 17.08 18.9651 17.5014C18.7953 17.9371 18.5688 18.3069 18.2002 18.6752C17.8321 19.0438 17.4629 19.2707 17.0272 19.44C16.6056 19.6039 16.1235 19.7156 15.4181 19.7479C14.7112 19.78 14.4861 19.7879 12.6863 19.7879C10.8867 19.7879 10.661 19.78 9.95418 19.7479C9.24886 19.7156 8.76719 19.6039 8.34558 19.44C7.91002 19.2707 7.54031 19.0438 7.17212 18.6752C6.80366 18.3069 6.57725 17.9371 6.40786 17.5013C6.24413 17.08 6.13244 16.5984 6.1 15.893C6.06797 15.1862 6.05996 14.961 6.05996 13.1613C6.05996 11.3617 6.06825 11.1357 6.09986 10.4288C6.13161 9.72365 6.24344 9.24199 6.40772 8.82051C6.57753 8.38481 6.80393 8.01511 7.17254 7.64678C7.54086 7.27831 7.91057 7.05191 8.34641 6.88265C8.76775 6.71878 9.24928 6.6071 9.95473 6.57479Z"
								fill="white"
							/>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M12.0926 7.72895C12.208 7.72877 12.3322 7.72883 12.4662 7.72889L12.6871 7.72895C14.4564 7.72895 14.6661 7.7353 15.3648 7.76705C16.0108 7.7966 16.3615 7.90456 16.5951 7.99526C16.9043 8.11536 17.1248 8.25894 17.3566 8.49087C17.5885 8.7228 17.7321 8.94368 17.8525 9.25292C17.9432 9.48623 18.0513 9.83689 18.0807 10.483C18.1124 11.1815 18.1193 11.3914 18.1193 13.1598C18.1193 14.9283 18.1124 15.1381 18.0807 15.8367C18.0511 16.4828 17.9432 16.8334 17.8525 17.0667C17.7324 17.376 17.5885 17.5962 17.3566 17.828C17.1247 18.0599 16.9045 18.2035 16.5951 18.3236C16.3618 18.4147 16.0108 18.5224 15.3648 18.5519C14.6662 18.5837 14.4564 18.5906 12.6871 18.5906C10.9176 18.5906 10.7079 18.5837 10.0094 18.5519C9.3633 18.5221 9.01264 18.4142 8.77892 18.3234C8.46968 18.2033 8.24879 18.0598 8.01686 17.8278C7.78493 17.5959 7.64136 17.3756 7.52097 17.0662C7.43027 16.8329 7.32218 16.4822 7.29277 15.8361C7.26102 15.1376 7.25467 14.9277 7.25467 13.1582C7.25467 11.3886 7.26102 11.1799 7.29277 10.4813C7.32232 9.83523 7.43027 9.48458 7.52097 9.25099C7.64108 8.94175 7.78493 8.72087 8.01686 8.48894C8.24879 8.25701 8.46968 8.11343 8.77892 7.99305C9.0125 7.90193 9.3633 7.79425 10.0094 7.76457C10.6207 7.73696 10.8576 7.72868 12.0926 7.72729V7.72895ZM16.2243 8.82928C15.7853 8.82928 15.4291 9.18504 15.4291 9.62419C15.4291 10.0632 15.7853 10.4194 16.2243 10.4194C16.6633 10.4194 17.0195 10.0632 17.0195 9.62419C17.0195 9.18518 16.6633 8.829 16.2243 8.829V8.82928ZM9.28405 13.1613C9.28405 11.2821 10.8077 9.75839 12.6869 9.75831C14.5663 9.75831 16.0895 11.282 16.0895 13.1613C16.0895 15.0407 14.5664 16.5637 12.6871 16.5637C10.8078 16.5637 9.28405 15.0407 9.28405 13.1613Z"
								fill="white"
							/>
							<path
								d="M12.6871 10.9524C13.9069 10.9524 14.8959 11.9413 14.8959 13.1612C14.8959 14.3811 13.9069 15.3701 12.6871 15.3701C11.4671 15.3701 10.4782 14.3811 10.4782 13.1612C10.4782 11.9413 11.4671 10.9524 12.6871 10.9524Z"
								fill="white"
							/>
						</svg>
					</li>
					<li>
						<svg
							width="25"
							height="26"
							viewBox="0 0 25 26"
							fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								fill-rule="evenodd"
								clip-rule="evenodd"
								d="M12.5753 0.736572C5.71335 0.736572 0.150635 6.29929 0.150635 13.1613C0.150635 20.0232 5.71335 25.5859 12.5753 25.5859C19.4373 25.5859 25 20.0232 25 13.1613C25 6.29929 19.4373 0.736572 12.5753 0.736572ZM12.2293 11.2655L12.2032 10.8355C12.125 9.72126 12.8116 8.70348 13.8979 8.30866C14.2977 8.16827 14.9756 8.15072 15.4188 8.27356C15.5926 8.3262 15.9229 8.50168 16.1575 8.65961L16.5834 8.94915L17.0527 8.8C17.3134 8.72103 17.661 8.58942 17.8175 8.50168C17.9652 8.42272 18.0956 8.37885 18.0956 8.40517C18.0956 8.55433 17.774 9.06321 17.5046 9.34398C17.1396 9.7388 17.2439 9.7739 17.9826 9.51068C18.4258 9.36153 18.4345 9.36153 18.3476 9.52823C18.2955 9.61597 18.026 9.92306 17.7392 10.2038C17.2526 10.6864 17.2265 10.739 17.2265 11.1426C17.2265 11.7656 16.931 13.0641 16.6355 13.7748C16.088 15.1084 14.9147 16.4859 13.7415 17.1791C12.0902 18.153 9.89144 18.3987 8.0403 17.8284C7.42325 17.6353 6.36298 17.144 6.36298 17.0563C6.36298 17.0299 6.68454 16.9948 7.07562 16.9861C7.89256 16.9685 8.70949 16.7404 9.40476 16.3368L9.87406 16.056L9.33523 15.8718C8.57044 15.6086 7.88387 15.0032 7.71005 14.4329C7.65791 14.2486 7.67529 14.2398 8.16197 14.2398L8.66604 14.2311L8.24019 14.0293C7.73612 13.7748 7.27551 13.3449 7.04955 12.9062C6.88442 12.5903 6.67585 11.7919 6.73668 11.7305C6.75406 11.7042 6.93657 11.7568 7.14515 11.827C7.74481 12.0463 7.82303 11.9937 7.4754 11.6252C6.82359 10.9584 6.6237 9.96693 6.93657 9.02812L7.08431 8.60697L7.65791 9.17727C8.83117 10.3267 10.213 11.011 11.7947 11.2128L12.2293 11.2655Z"
								fill="white"
							/>
						</svg>
					</li>
				</ul>
			</div>
		</footer>
	</body>

	<script
		src="https://code.jquery.com/jquery-3.4.1.js"
		integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
		crossorigin="anonymous"
	></script>
	<script
		src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
		integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
		crossorigin="anonymous"
	></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    <script src="<?= get_template_directory_uri()?>/assets/js/app.js"></script>
</html>
