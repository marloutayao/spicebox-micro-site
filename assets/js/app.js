$(document).ready(function () {
	// Get the modal
	var modal = document.getElementById("confirmation-modal");
	var first_modal = document.getElementById("first-show-modal");

	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks on <span> (x), close the modal
	span.onclick = function () {
		modal.style.display = "none";
	};

	// Get the <span> element that closes the modal
	var span1 = document.getElementsByClassName("close")[1];

	// When the user clicks on <span> (x), close the modal
	span1.onclick = function () {
		first_modal.style.display = "none";
	};

	$(".same-pop-up").click(function () {
		first_modal.style.display = "none";
	});

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function (event) {
		if (event.target == modal) {
			modal.style.display = "none";
		}
	};

	$("#kashmir_delivery").click(function () {
		$(".show_when_kashmir").css("display", "block");

		$("#final_total_price").html(
			$("#sub_total_price").html() * 1 +
				$("#delivery_charge_price").html() * 1
		);
	});

	$("#makati-bgc").click(function () {
		$("#delivery_charge_price").html("80");
		$("#final_total_price").html(
			$("#sub_total_price").html() * 1 +
				$("#delivery_charge_price").html() * 1
		);
	});

	$("#other-place").click(function () {
		$("#delivery_charge_price").html("0");
		$("#final_total_price").html(
			$("#sub_total_price").html() * 1 +
				$("#delivery_charge_price").html() * 1
		);
	});

	$("#lalamove").click(function () {
		$(".show_when_kashmir").css("display", "none");
		$("#other-place").prop("checked", false);
		$("#makati-bgc").prop("checked", false);
		$("#delivery_charge_price").html("0");
		$("#final_total_price").html(
			$("#sub_total_price").html() * 1 +
				$("#delivery_charge_price").html() * 1
		);
	});
	$("#grab_express").click(function () {
		$(".show_when_kashmir").css("display", "none");
		$("#makati-bgc").prop("checked", false);
		$("#other-place").prop("checked", false);
		$("#delivery_charge_price").html("0");
		$("#final_total_price").html(
			$("#sub_total_price").html() * 1 +
				$("#delivery_charge_price").html() * 1
		);
	});
	$("#pick_up_yes").click(function () {
		$(".show_when_delivery").css("display", "none");
	});
	$("#pick_up_no").click(function () {
		$(".show_when_delivery").css("display", "block");
	});

	$("#BDO").click(function () {
		$("#bdo-information").css("display", "block");
		$("#unionbank-information").css("display", "none");
		$("#gcash-information").css("display", "none");
	});
	$("#UnionBank").click(function () {
		$("#bdo-information").css("display", "none");
		$("#unionbank-information").css("display", "block");
		$("#gcash-information").css("display", "none");
	});
	$("#Gcash").click(function () {
		$("#bdo-information").css("display", "none");
		$("#unionbank-information").css("display", "none");
		$("#gcash-information").css("display", "block");
	});
	$("#swipe_on_delivery").click(function () {
		$("#bdo-information").css("display", "none");
		$("#unionbank-information").css("display", "none");
		$("#gcash-information").css("display", "none");
	});
	let all_orders = "";
	let total_price = 0;
	// VEGGIE
	let dal_chana = "",
		punjabi_dal = "",
		chola = "",
		palak_paneer = "",
		paneer_makhani = "",
		kadai_paneer = "",
		veg_curry = "",
		hummus = "",
		eggplant_casserole = "",
		plain_biryani = "",
		veg_biryani = "";
	let dal_chana_price = 0,
		punjabi_dal_price = 0,
		chola_price = 0,
		palak_paneer_price = 0,
		paneer_makhani_price = 0,
		kadai_paneer_price = 0,
		veg_curry_price = 0,
		hummus_price = 0,
		eggplant_casserole_price = 0,
		plain_biryani_price = 0,
		veg_biryani_price = 0;
	// END OF VEGGIE

	// NON-VEGGIE
	let chicken_tikka_masala = "",
		kadai_chicken = "",
		beef_curry = "",
		rogan_josh = "",
		chicken_biryani = "",
		mutton_biryani = "",
		beef_biryani = "",
		chicken_shawarma = "",
		beef_shawarma = "";
	let chicken_tikka_masala_price = 0,
		kadai_chicken_price = 0,
		beef_curry_price = 0,
		rogan_josh_price = 0,
		chicken_biryani_price = 0,
		mutton_biryani_price = 0,
		beef_biryani_price = 0,
		chicken_shawarma_price = 0,
		beef_shawarma_price = 0;
	// END OF NON-VEGGIE
	// frozen offerings
	let samosa_pack_a = "",
		samosa_pack_b = "",
		samosa_pack_c = "",
		naan = "",
		roti_chana = "",
		pita_bread = "",
		chapati = "";
	let samosa_pack_a_price = 0,
		samosa_pack_b_price = 0,
		samosa_pack_c_price = 0,
		naan_price = 0,
		roti_chana_price = 0,
		pita_bread_price = 0,
		chapati_price = 0;
	// end of frozen offerings
	//let thali bilao
	let thali_non_veg = "",
		thali_veg = "";
	let thali_non_veg_price = 0,
		thali_veg_price = 0;
	// end of thali

	$("#veg").click(function () {
		$("#vegetarian-menu").css("display", "block");
		$("#non-vegetarian-menu").css("display", "none");
		$("#frozen-offerings-menu").css("display", "none");
		$("#thali-bilao-to-go-menu").css("display", "none");
	});

	$("#non-veg").click(function () {
		$("#non-vegetarian-menu").css("display", "block");
		$("#vegetarian-menu").css("display", "none");
		$("#frozen-offerings-menu").css("display", "none");
		$("#thali-bilao-to-go-menu").css("display", "none");
	});

	$("#frozen-offerings").click(function () {
		$("#frozen-offerings-menu").css("display", "block");
		$("#vegetarian-menu").css("display", "none");
		$("#non-vegetarian-menu").css("display", "none");
		$("#thali-bilao-to-go-menu").css("display", "none");
	});
	$("#thali-bilao-to-go").click(function () {
		$("#thali-bilao-to-go-menu").css("display", "block");
		$("#vegetarian-menu").css("display", "none");
		$("#non-vegetarian-menu").css("display", "none");
		$("#frozen-offerings-menu").css("display", "none");
	});

	function enable_disable_input(checkbox_name, input_name) {
		if ($(checkbox_name).prop("checked")) {
			$(input_name).prop("disabled", false);
		} else {
			$(input_name).prop("disabled", true);
			$(input_name).val("");
		}
	}

	function get_total_price() {
		total_price =
			dal_chana_price +
			punjabi_dal_price +
			chola_price +
			palak_paneer_price +
			paneer_makhani_price +
			kadai_paneer_price +
			+veg_curry_price +
			hummus_price +
			eggplant_casserole_price +
			plain_biryani_price +
			veg_biryani_price +
			chicken_tikka_masala_price +
			kadai_chicken_price +
			beef_curry_price +
			rogan_josh_price +
			chicken_biryani_price +
			mutton_biryani_price +
			beef_biryani_price +
			chicken_shawarma_price +
			beef_shawarma_price +
			samosa_pack_a_price +
			samosa_pack_b_price +
			samosa_pack_c_price +
			naan_price +
			roti_chana_price +
			pita_bread_price +
			chapati_price +
			thali_non_veg_price +
			thali_veg_price;

		$("#total_price").html(total_price);
		$("#sub_total_price").html(total_price);
	}

	function get_all_orders() {
		all_orders =
			dal_chana +
			punjabi_dal +
			chola +
			palak_paneer +
			paneer_makhani +
			kadai_paneer +
			veg_curry +
			hummus +
			eggplant_casserole +
			plain_biryani +
			veg_biryani +
			chicken_tikka_masala +
			kadai_chicken +
			beef_curry +
			rogan_josh +
			chicken_biryani +
			mutton_biryani +
			beef_biryani +
			chicken_shawarma +
			beef_shawarma +
			samosa_pack_a +
			samosa_pack_b +
			samosa_pack_c +
			naan +
			roti_chana +
			pita_bread +
			chapati +
			thali_non_veg +
			thali_veg;
	}

	$("#thali-bilao-to-go1").click(function () {
		enable_disable_input(
			"#thali-bilao-to-go1",
			"#thali-bilao-to-go1-quantity"
		);
		thali_non_veg = "";
		thali_non_veg_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#thali-bilao-to-go1-quantity").change(function () {
		samosa_pack_a =
			$("thali-bilao-to-go1").val() + " " + $(this).val() + "pcs. \n";
		thali_non_veg_price = 0;
		thali_non_veg_price = $(this).val() * 2100;
		get_total_price();
		get_all_orders();
	});

	$("#thali-bilao-to-go2").click(function () {
		enable_disable_input(
			"#thali-bilao-to-go2",
			"#thali-bilao-to-go2-quantity"
		);
		thali_veg = "";
		thali_veg_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#thali-bilao-to-go2-quantity").change(function () {
		samosa_pack_a =
			$("thali-bilao-to-go2").val() + " " + $(this).val() + "pcs. \n";
		thali_veg_price = 0;
		thali_veg_price = $(this).val() * 1900;
		get_total_price();
		get_all_orders();
	});

	$("#frozen-offerings1").click(function () {
		enable_disable_input(
			"#frozen-offerings1",
			"#frozen-offerings1-quantity"
		);
		samosa_pack_a = "";
		samosa_pack_a_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#frozen-offerings1-quantity").change(function () {
		samosa_pack_a =
			$("#frozen-offerings1").val() + " " + $(this).val() + "pcs. \n";
		samosa_pack_a_price = 0;
		samosa_pack_a_price = $(this).val() * 1080;
		get_total_price();
		get_all_orders();
	});

	$("#frozen-offerings2").click(function () {
		enable_disable_input(
			"#frozen-offerings2",
			"#frozen-offerings2-quantity"
		);
		samosa_pack_b = "";
		samosa_pack_b_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#frozen-offerings2-quantity").change(function () {
		samosa_pack_b =
			$("#frozen-offerings2").val() + " " + $(this).val() + "pcs. \n";
		samosa_pack_b_price = 0;
		samosa_pack_b_price = $(this).val() * 720;
		get_total_price();
		get_all_orders();
	});

	$("#frozen-offerings3").click(function () {
		enable_disable_input(
			"#frozen-offerings3",
			"#frozen-offerings3-quantity"
		);
		samosa_pack_c = "";
		samosa_pack_c_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#frozen-offerings3-quantity").change(function () {
		samosa_pack_c =
			$("#frozen-offerings3").val() + " " + $(this).val() + "pcs. \n";
		samosa_pack_c_price = 0;
		samosa_pack_c_price = $(this).val() * 1080;
		get_total_price();
		get_all_orders();
	});

	$("#frozen-offerings4").click(function () {
		enable_disable_input(
			"#frozen-offerings4",
			"#frozen-offerings4-quantity"
		);
		naan = "";
		naan_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#frozen-offerings4-quantity").change(function () {
		naan = $("#frozen-offerings4").val() + " " + $(this).val() + "pcs. \n";
		naan_price = 0;
		naan_price = $(this).val() * 300;
		get_total_price();
		get_all_orders();
	});

	$("#frozen-offerings5").click(function () {
		enable_disable_input(
			"#frozen-offerings5",
			"#frozen-offerings5-quantity"
		);
		roti_chana = "";
		roti_chana_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#frozen-offerings5-quantity").change(function () {
		roti_chana =
			$("#frozen-offerings5").val() + " " + $(this).val() + "pcs. \n";
		roti_chana_price = 0;
		roti_chana_price = $(this).val() * 360;
		get_total_price();
		get_all_orders();
	});

	$("#frozen-offerings6").click(function () {
		enable_disable_input(
			"#frozen-offerings6",
			"#frozen-offerings6-quantity"
		);
		pita_bread = "";
		pita_bread_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#frozen-offerings6-quantity").change(function () {
		pita_bread =
			$("#frozen-offerings6").val() + " " + $(this).val() + "pcs. \n";
		pita_bread_price = 0;
		pita_bread_price = $(this).val() * 150;
		get_total_price();
		get_all_orders();
	});

	$("#frozen-offerings7").click(function () {
		enable_disable_input(
			"#frozen-offerings7",
			"#frozen-offerings7-quantity"
		);
		chapati = "";
		chapati_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#frozen-offerings7-quantity").change(function () {
		chapati =
			$("#frozen-offerings7").val() + " " + $(this).val() + "pcs. \n";
		chapati_price = 0;
		chapati_price = $(this).val() * 150;
		get_total_price();
		get_all_orders();
	});

	// non veggi
	$("#non-veg1").click(function () {
		enable_disable_input("#non-veg1", "#non-veg1-quantity");
		chicken_tikka_masala_price = 0;
		chicken_tikka_masala = "";
		get_total_price();
		get_all_orders();
	});
	$("#non-veg1-quantity").change(function () {
		chicken_tikka_masala =
			$("#non-veg1").val() + " " + $(this).val() + "pcs. \n";
		chicken_tikka_masala_price = 0;
		chicken_tikka_masala_price = $(this).val() * 440;
		get_total_price();
		get_all_orders();
	});

	$("#non-veg2").click(function () {
		enable_disable_input("#non-veg2", "#non-veg2-quantity");
		kadai_chicken_price = 0;
		kadai_chicken = "";
		get_total_price();
		get_all_orders();
	});
	$("#non-veg2-quantity").change(function () {
		kadai_chicken = $("#non-veg2").val() + " " + $(this).val() + "pcs. \n";
		kadai_chicken_price = 0;
		kadai_chicken_price = $(this).val() * 440;
		get_total_price();
		get_all_orders();
	});

	$("#non-veg3").click(function () {
		enable_disable_input("#non-veg3", "#non-veg3-quantity");
		beef_curry_price = 0;
		beef_curry = "";
		get_total_price();
		get_all_orders();
	});
	$("#non-veg3-quantity").change(function () {
		beef_curry = $("#non-veg3").val() + " " + $(this).val() + "pcs. \n";
		beef_curry_price = 0;
		beef_curry_price = $(this).val() * 500;
		get_total_price();
		get_all_orders();
	});

	$("#non-veg4").click(function () {
		enable_disable_input("#non-veg4", "#non-veg4-quantity");
		rogan_josh_price = 0;
		rogan_josh = "";
		get_total_price();
		get_all_orders();
	});
	$("#non-veg4-quantity").change(function () {
		rogan_josh = $("#non-veg4").val() + " " + $(this).val() + "pcs. \n";
		rogan_josh_price = 0;
		rogan_josh_price = $(this).val() * 650;
		get_total_price();
		get_all_orders();
	});

	$("#non-veg5").click(function () {
		enable_disable_input("#non-veg5", "#non-veg5-quantity");
		chicken_biryani = "";
		chicken_biryani_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#non-veg5-quantity").change(function () {
		chicken_biryani =
			$("#non-veg5").val() + " " + $(this).val() + "pcs. \n";
		chicken_biryani_price = 0;
		chicken_biryani_price = $(this).val() * 680;
		get_total_price();
		get_all_orders();
	});

	$("#non-veg6").click(function () {
		enable_disable_input("#non-veg6", "#non-veg6-quantity");
		mutton_biryani = "";
		mutton_biryani_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#non-veg6-quantity").change(function () {
		mutton_biryani = $("#non-veg6").val() + " " + $(this).val() + "pcs. \n";
		mutton_biryani_price = 0;
		mutton_biryani_price = $(this).val() * 690;
		get_total_price();
		get_all_orders();
	});

	$("#non-veg7").click(function () {
		enable_disable_input("#non-veg7", "#non-veg7-quantity");
		beef_biryani_price = 0;
		beef_biryani = "";
		get_total_price();
		get_all_orders();
	});
	$("#non-veg7-quantity").change(function () {
		beef_biryani = $("#non-veg7").val() + " " + $(this).val() + "pcs. \n";
		beef_biryani_price = 0;
		beef_biryani_price = $(this).val() * 690;
		get_total_price();
		get_all_orders();
	});

	$("#non-veg8").click(function () {
		enable_disable_input("#non-veg8", "#non-veg8-quantity");
		chicken_shawarma = "";
		chicken_shawarma_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#non-veg8-quantity").change(function () {
		chicken_shawarma =
			$("#non-veg8").val() + " " + $(this).val() + "pcs. \n";
		chicken_shawarma_price = 0;
		chicken_shawarma_price = $(this).val() * 220;
		get_total_price();
		get_all_orders();
	});
	$("#non-veg9").click(function () {
		enable_disable_input("#non-veg9", "#non-veg9-quantity");
		beef_shawarma = "";
		beef_shawarma_price = 0;
		get_total_price();
		get_all_orders();
	});
	$("#non-veg9-quantity").change(function () {
		beef_shawarma = $("#non-veg9").val() + " " + $(this).val() + "pcs. \n";
		beef_shawarma_price = 0;
		beef_shawarma_price = $(this).val() * 220;
		get_total_price();
		get_all_orders();
	});
	// end of non veggie

	// VEGGIE MENU
	$("#veg1").click(function () {
		enable_disable_input("#veg1", "#veg1-quantity");
		dal_chana_price = 0;
		dal_chana = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg1-quantity").change(function () {
		dal_chana = $("#veg1").val() + " " + $(this).val() + "pcs. \n";
		dal_chana_price = 0;
		dal_chana_price = $(this).val() * 400;
		get_total_price();
		get_all_orders();
	});

	$("#veg2").click(function () {
		enable_disable_input("#veg2", "#veg2-quantity");
		punjabi_dal_price = 0;
		punjabi_dal = "";
		get_total_price();
		get_all_orders();
	});

	$("#veg2-quantity").change(function () {
		punjabi_dal = $("#veg2").val() + " " + $(this).val() + "pcs. \n";
		punjabi_dal_price = 0;
		punjabi_dal_price = $(this).val() * 400;
		get_total_price();
		get_all_orders();
	});

	$("#veg3").click(function () {
		enable_disable_input("#veg3", "#veg3-quantity");
		chola_price = 0;
		chola = "";
		get_total_price();
		get_all_orders();
	});

	$("#veg3-quantity").change(function () {
		chola = $("#veg3").val() + " " + $(this).val() + "pcs. \n";
		chola_price = 0;
		chola_price = $(this).val() * 350;
		get_total_price();
		get_all_orders();
	});

	$("#veg4").click(function () {
		enable_disable_input("#veg4", "#veg4-quantity");
		palak_paneer_price = 0;
		palak_paneer = "";
		get_total_price();
		get_all_orders();
	});

	$("#veg4-quantity").change(function () {
		palak_paneer = $("#veg4").val() + " " + $(this).val() + "pcs. \n";
		palak_paneer_price = 0;
		palak_paneer_price = $(this).val() * 420;
		get_total_price();
		get_all_orders();
	});

	$("#veg5").click(function () {
		enable_disable_input("#veg5", "#veg5-quantity");
		paneer_makhani_price = 0;
		paneer_makhani = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg5-quantity").change(function () {
		paneer_makhani = $("#veg5").val() + " " + $(this).val() + "pcs. \n";
		paneer_makhani_price = 0;
		paneer_makhani_price = $(this).val() * 420;
		get_total_price();
		get_all_orders();
	});

	$("#veg6").click(function () {
		enable_disable_input("#veg6", "#veg6-quantity");
		kadai_paneer_price = 0;
		kadai_paneer = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg6-quantity").change(function () {
		kadai_paneer = $("#veg6").val() + " " + $(this).val() + "pcs. \n";
		kadai_paneer_price = 0;
		kadai_paneer_price = $(this).val() * 420;
		get_total_price();
		get_all_orders();
	});

	$("#veg7").click(function () {
		enable_disable_input("#veg7", "#veg7-quantity");
		veg_curry_price = 0;
		veg_curry = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg7-quantity").change(function () {
		veg_curry = $("#veg7").val() + " " + $(this).val() + "pcs. \n";
		veg_curry_price = 0;
		veg_curry_price = $(this).val() * 400;
		get_total_price();
		get_all_orders();
	});

	$("#veg8").click(function () {
		enable_disable_input("#veg8", "#veg8-quantity");
		hummus_price = 0;
		hummus = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg8-quantity").change(function () {
		hummus = $("#veg8").val() + " " + $(this).val() + "pcs. \n";
		hummus_price = 0;
		hummus_price = $(this).val() * 340;
		get_total_price();
		get_all_orders();
	});

	$("#veg9").click(function () {
		enable_disable_input("#veg9", "#veg9-quantity");
		eggplant_casserole_price = 0;
		eggplant_casserole = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg9-quantity").change(function () {
		eggplant_casserole = $("#veg9").val() + " " + $(this).val() + "pcs. \n";
		eggplant_casserole_price = 0;
		eggplant_casserole_price = $(this).val() * 400;
		get_total_price();
		get_all_orders();
	});

	$("#veg10").click(function () {
		enable_disable_input("#veg10", "#veg10-quantity");
		plain_biryani_price = 0;
		plain_biryani = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg10-quantity").change(function () {
		plain_biryani = $("#veg10").val() + " " + $(this).val() + "pcs. \n";
		plain_biryani_price = 0;
		plain_biryani_price = $(this).val() * 250;
		get_total_price();
		get_all_orders();
	});

	$("#veg11").click(function () {
		enable_disable_input("#veg11", "#veg11-quantity");
		veg_biryani_price = 0;
		veg_biryani = "";
		get_total_price();
		get_all_orders();
	});
	$("#veg11-quantity").change(function () {
		veg_biryani = $("#veg11").val() + " " + $(this).val() + "pcs. \n";
		veg_biryani_price = 0;
		veg_biryani_price = $(this).val() * 420;
		get_total_price();
		get_all_orders();
	});
	// end of veggie menu

	$("#item_get_date")
		.datepicker({
			dateFormat: "DD, d MM, yy",
		})
		.datepicker("setDate", new Date());

	$("#item_get_date").click(function () {
		console.log($(this).val());
	});

	$("#item_get_time").timepicker({
		minTime: "11:30am",
		maxTime: "5:30pm",
		scrollbar: true,
		dropdown: true,
		defaultTime: "11:30am",
	});

	$(".equal-cards").click(function () {
		if ($(this).attr("id") == 1) {
			window.open("#", "_blank");
		}

		if ($(this).attr("id") == 2) {
			window.open("#", "_blank");
		}

		if ($(this).attr("id") == 3) {
			window.open("#", "_blank");
		}
	});

	$("#submit_order").click(function () {
		modal.style.display = "flex";
		let price = total_price;
		let orders = all_orders;
		let name = $("#user_name").val();
		let user_email = $("#user_email").val();
		let opt_in = $("input:checkbox[name='entry.857768510']:checked").val();
		let contact_number = $("#user_contact_number").val();
		let address = $("#user_address").val();
		let pick_up = $("input:radio[name='entry.1971904388']:checked").val();
		// let delivery = $("input:radio[name='entry.738571523']:checked").val();
		let get_date = $("#item_get_date").val();
		let get_time = $("#item_get_time").val();
		let shipping_method = $(
			"input:radio[name='entry.1914199105']:checked"
		).val();
		let payment_method = $(
			"input:radio[name='entry.1092826214']:checked"
		).val();
		let error = false;

		if (name == "") {
			alert("Please, input your name");
			document.getElementById("user_name").focus();
			error = true;
		} else if (user_email == "") {
			alert("Please, input email address");
			document.getElementById("user_email").focus();
			error = true;
		} else if (contact_number == "") {
			alert("Please, input contact your number");
			document.getElementById("user_contact_number").focus();
			error = true;
		} else if (address == "") {
			alert("Please, input your address");
			document.getElementById("user_address").focus();
			error = true;
		}

		if (!error) {
			$.ajax({
				url:
					"https://docs.google.com/forms/u/0/d/e/1FAIpQLScA2milO6dsRnRg-Sbr3YU_sVxEB-b3ECFn3LV4zQMJqK9lrQ/formResponse",
				type: "POST",
				dataType: "xml",
				data: {
					"entry.2097966281": orders,
					"entry.1699876150": price,
					"entry.1016694887": name,
					"entry.1931345053": user_email,
					"entry.857768510": opt_in,
					"entry.437583099": contact_number,
					"entry.1103284590": address,
					"entry.1971904388": pick_up,
					"entry.638532062": get_date,
					"entry.1207434294": get_time,
					// "entry.738571523": delivery,
					"entry.1914199105": shipping_method,
					"entry.1092826214": payment_method,
				},
				success: function (response) {
					alert(
						"Order Success, please wait for our text/call. Thank you!"
					);
					window.location.reload();
				},
				error: function (x, y, z) {
					alert(
						"Order Success, please wait for our text/call. Thank you!"
					);
					window.location.reload();
				},
			});
		}
	});
});
